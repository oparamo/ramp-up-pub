package RampUpPub;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    private final Logger logger = LoggerFactory.getLogger(GreetingController.class);

    @Autowired
    private QueuePublisher publisher;

    @RequestMapping(value = "/greetings", method = RequestMethod.POST)
    public ResponseEntity<Greeting> postGreeting(@RequestBody Greeting greeting) {
        logger.info("POST /greetings");

        String content = greeting.getContent();
        if (content == null || content.isEmpty()) {
            content = "World";
        }

        greeting.setId(UUID.randomUUID());
        greeting.updateContent(content);

        publisher.sendMessage(greeting);

        return new ResponseEntity<Greeting>(greeting, HttpStatus.CREATED);
    }
}
