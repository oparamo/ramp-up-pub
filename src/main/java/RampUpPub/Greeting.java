package RampUpPub;

import java.util.UUID;

public class Greeting {
    private UUID id;
    private String content;

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void updateContent(String name) {
        this.content = String.format("Hello, %s!", name);
    }
}
