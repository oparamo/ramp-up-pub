package RampUpPub;

import javax.jms.Session;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class QueuePublisher {
    private final Logger logger = LoggerFactory.getLogger(QueuePublisher.class);

    private static Gson gson = new Gson();

    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${queue.QUEUE}")
    private String queue;

    public void sendMessage(Greeting greeting) {
        logger.info("Publishing Greeting with content: " + greeting.getContent());

        jmsTemplate.send(queue, (Session session) -> session.createTextMessage(gson.toJson(greeting)));
    }
}
